# SimpleWebServ

SimpleWebServ is a HTTP server made to make hosting websites easy for anyone. just put your index.html and other website files inside of a folder named /website

# Config
You will also need to have a config file. Simplewebserv supports and requires two entries into the config file.

PORT and VERBOOSE

the port option in the config file is the port your website will be hosted on. example,

PORT = 80

You also need to have verbose in the config file.
this is good for debugging and stuff normally i would recommend to have it disabled.

example

verbose = FALSE.


SimpleWebServ, a simple to use HTTP web server
   Copyright (C) 2020  a8_

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>.
