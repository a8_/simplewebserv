/*
SimpleWebServ, a simple to use HTTP web server
   Copyright (C) 2020  a8_

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/

package me.a8;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Date;
import java.util.Properties;
import java.util.StringTokenizer;
//based from https://www.ssaurel.com/blog/create-a-simple-http-web-server-in-java
//i fixed CSS and also added config file and made it to look for website in website folder 

// Each Client Connection will be managed in a dedicated Thread
public class JavaHTTPServer implements Runnable{





    public static int getPortFromProp() throws IOException {
        FileInputStream fis = new FileInputStream("./config.properties");
        Properties p = new Properties();
        p.load(fis);
        String Cport = p.getProperty("PORT");
        int pint = Integer.parseInt(Cport);
        return pint;
    }

    public static boolean getVerbooseFromProp() throws IOException {
        FileInputStream fis = new FileInputStream("./config.properties");
        Properties p = new Properties();
        p.load(fis);
        String pVerbS = p.getProperty("verbose");
        boolean pVerb = Boolean.parseBoolean(pVerbS);
        return pVerb;
    }



    static  File WEB_ROOT = new File("./website");
    static  String DEFAULT_FILE = "index.html";
    static  String FILE_NOT_FOUND = "404.html";
    static  String METHOD_NOT_SUPPORTED = "not_supported.html";
    //for prop file
    //static Properties p = new Properties();




    // port to listen connection
    static  int PORT;

    static {
        try {
            PORT = getPortFromProp();
        } catch (IOException e) {
            System.out.println("please make config file. reffer to example at https://tinyurl.com/uq57oku");
            System.out.println();
            e.printStackTrace();
            System.out.println();
            System.out.println("please make config file. reffer to example at https://tinyurl.com/uq57oku");
        }
    }

    // verbose mode
    static  boolean verbose;

    static {
        try {
            verbose = getVerbooseFromProp();
        } catch (IOException e) {
            System.out.println("please make config file. reffer to example at https://tinyurl.com/uq57oku");
            System.out.println();
            e.printStackTrace();
            System.out.println();
            System.out.println("please make config file. reffer to example at https://tinyurl.com/uq57oku");
        }
    }

    // Client Connection via Socket Class
    private Socket connect;

    public JavaHTTPServer(Socket c) throws FileNotFoundException {
        connect = c;
    }





    public static void main(String[] args) throws IOException {
        System.out.println("SimpleWebServ  Copyright (C) 2020  a8_");
        System.out.println("This program comes with ABSOLUTELY NO WARRANTY; for details read the gpl-3.0.md file that came with this program.");
        System.out.println("This is free software, and you are welcome to redistribute it");
        System.out.println();
        System.out.println("Verboose mode = " + verbose);


        try {
            ServerSocket serverConnect = new ServerSocket(PORT);
            System.out.println("Server started.\nListening for connections on port : " + PORT + " ...\n");

            // we listen until user halts server execution
            while (true) {
                JavaHTTPServer myServer = new JavaHTTPServer(serverConnect.accept());

                if (verbose) {
                    System.out.println("Connecton opened. (" + new Date() + ")");
                }

                // create dedicated thread to manage the client connection
                Thread thread = new Thread(myServer);
                thread.start();
            }

        } catch (IOException e) {
            System.err.println("Server Connection error : " + e.getMessage());
        }
    }

    @Override
    public void run() {



        // we manage our particular client connection
        BufferedReader in = null; PrintWriter out = null; BufferedOutputStream dataOut = null;
        String fileRequested = null;

        try {
            // we read characters from the client via input stream on the socket
            in = new BufferedReader(new InputStreamReader(connect.getInputStream()));
            // we get character output stream to client (for headers)
            out = new PrintWriter(connect.getOutputStream());
            // get binary output stream to client (for requested data)
            dataOut = new BufferedOutputStream(connect.getOutputStream());

            // get first line of the request from the client
            String input = in.readLine();
            // we parse the request with a string tokenizer
            StringTokenizer parse = new StringTokenizer(input);
            String method = parse.nextToken().toUpperCase(); // we get the HTTP method of the client
            // we get file requested
            fileRequested = parse.nextToken();

            // we support only GET and HEAD methods, we check
            if (!method.equals("GET")  &&  !method.equals("HEAD")) {
                if (verbose) {
                    System.out.println("501 Not Implemented : " + method + " method.");
                }

                // we return the not supported file to the client
                File file = new File(WEB_ROOT, METHOD_NOT_SUPPORTED);
                int fileLength = (int) file.length();
                String contentMimeType = "text/html";
                //read content to return to client
                byte[] fileData = readFileData(file, fileLength);

                // we send HTTP Headers with data to client
                out.println("HTTP/1.1 501 Not Implemented");
                out.println("Server: Simple web server 1.0 By a8_");
                out.println("Date: " + new Date());
                out.println("Content-type: " + contentMimeType);
                out.println("Content-length: " + fileLength);
                out.println(); // blank line between headers and content, very important !
                out.flush(); // flush character output stream buffer
                // file
                dataOut.write(fileData, 0, fileLength);
                dataOut.flush();

            } else {
                // GET or HEAD method
                if (fileRequested.endsWith("/")) {
                    fileRequested += DEFAULT_FILE;
                }

                File file = new File(WEB_ROOT, fileRequested);
                int fileLength = (int) file.length();
                String content = getContentType(fileRequested);

                if (method.equals("GET")) { // GET method so we return content
                    byte[] fileData = readFileData(file, fileLength);

                    // send HTTP Headers
                    out.println("HTTP/1.1 200 OK");
                    out.println("Server: Simple web server 1.0 By a8_");
                    out.println("Date: " + new Date());
                    out.println("Content-type: " + content);
                    out.println("Content-length: " + fileLength);
                    out.println(); // blank line between headers and content, very important !
                    out.flush(); // flush character output stream buffer

                    dataOut.write(fileData, 0, fileLength);
                    dataOut.flush();
                }

                if (verbose) {
                    System.out.println("File " + fileRequested + " of type " + content + " returned");
                }

            }

        } catch (FileNotFoundException fnfe) {
            try {
                fileNotFound(out, dataOut, fileRequested);
            } catch (IOException ioe) {
                System.err.println("Error with file not found exception : " + ioe.getMessage());
            }

        } catch (IOException ioe) {
            System.err.println("Server error : " + ioe);
        } finally {
            try {
                in.close();
                out.close();
                dataOut.close();
                connect.close(); // we close socket connection
            } catch (Exception e) {
                System.err.println("Error closing stream : " + e.getMessage());
            }

            if (verbose) {
                System.out.println("Connection closed.\n");
            }
        }


    }

    private byte[] readFileData(File file, int fileLength) throws IOException {
        FileInputStream fileIn = null;
        byte[] fileData = new byte[fileLength];

        try {
            fileIn = new FileInputStream(file);
            fileIn.read(fileData);
        } finally {
            if (fileIn != null)
                fileIn.close();
        }

        return fileData;
    }

    // return supported MIME Types
    private String getContentType(String fileRequested) {
        if (fileRequested.endsWith(".css")){
            return "text/css";
        }
        if (fileRequested.endsWith(".htm")  ||  fileRequested.endsWith(".html")) {
            return "text/html";}
        else {
            return "text/plain";
        }



    }



    private void fileNotFound(PrintWriter out, OutputStream dataOut, String fileRequested) throws IOException {
        File file = new File(WEB_ROOT, FILE_NOT_FOUND);
        int fileLength = (int) file.length();
        String content = "text/html";
        byte[] fileData = readFileData(file, fileLength);

        out.println("HTTP/1.1 404 File Not Found");
        out.println("Server: Simple web server 1.0 By a8_");
        out.println("Date: " + new Date());
        out.println("Content-type: " + content);
        out.println("Content-length: " + fileLength);
        out.println(); // blank line between headers and content, very important !
        out.flush(); // flush character output stream buffer

        dataOut.write(fileData, 0, fileLength);
        dataOut.flush();

        if (verbose) {
            System.out.println("File " + fileRequested + " not found");
        }
    }

}
